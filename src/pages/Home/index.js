import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import MapView, { Marker } from 'react-native-maps';

const Home = () => {
    return (
        <View style={styles.container}>
            <MapView
                style={styles.map}
                initialRegion={{
                    latitude: -2.9732686,
                    longitude: 104.7198696,
                    latitudeDelta: 0.002,
                    longitudeDelta: 0.002,
                }}
            >
                <Marker coordinate={{ latitude: -2.9731555378099914, longitude: 104.72040230590665 }} />
            </MapView>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
        flex: 1
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    logoutButton: {
        alignItems: 'center',
        marginBottom: 585,
        justifyContent: 'center',
        marginRight: 320
    },
    buttonContainer: {
        backgroundColor: 'black',
        width: 55,
        height: 55,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30
    },
    loginText: {
        color: 'white'
    },
})