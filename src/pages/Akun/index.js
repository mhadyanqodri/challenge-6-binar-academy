import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import auth from '@react-native-firebase/auth'

const Akun = () => {

    const logout = () => {
        auth()
            .signOut()
            .then(() => {navigation.replace('NeedLogin')})
            alert("sukses")
    }

    return (
        <View style={styles.logoutButton}>
            <TouchableOpacity onPress={() => logout()}>
                <View style={styles.buttonContainer}>
                    <Text style={styles.loginText}>Logout</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default Akun

const styles = StyleSheet.create({
    logoutButton: {
        alignItems: 'center',
        marginBottom: 500,
        justifyContent: 'center',
        flex: 1
    },
    buttonContainer: {
        backgroundColor: 'black',
        width: 280,
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8
    },
    loginText: {
        color: 'white'
    },
})