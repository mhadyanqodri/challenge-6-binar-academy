import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { CameraScreen, CameraType } from 'react-native-camera-kit';

const Camera = () => {
  return (
    <View>
      <CameraScreen
        cameraType={CameraType.Front} // front/back(default)
        // Barcode props
        scanBarcode={true}
        onReadCode={(event) => Alert.alert('QR code found')} // optional
        showFrame={true} // (default false) optional, show frame with transparent layer (qr code or barcode will be read on this area ONLY), start animation for scanner,that stoped when find any code. Frame always at center of the screen
        laserColor='red' // (default red) optional, color of laser in scanner frame
        frameColor='white' // (default white) optional, color of border of scanner frame
      />
    </View>
  )
}

export default Camera

const styles = StyleSheet.create({})