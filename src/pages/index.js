import Splash from "./Splash";
import Login from "./Login";
import Home from "./Home";
import Camera from "./Camera";
import Akun from "./Akun";

export { Splash, Login, Home, Camera, Akun };