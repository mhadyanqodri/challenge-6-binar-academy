import IconHome from './Home.svg'
import IconHomeActive from './HomeActive.svg'
import IconCamera from './DaftarMobil.svg'
import IconCameraActive from './DaftarMobilActive.svg'
import IconAkun from './Akun.svg'
import IconAkunActive from './AkunActive.svg'

export { 
    IconHome,
    IconHomeActive,
    IconCamera, 
    IconCameraActive,
    IconAkun, 
    IconAkunActive
}